﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        float num1, ans;
        int count;



        private void ClearButton_Click(object sender, EventArgs e)
        {
            OutputBox.Clear();
            count = 0;
            
        }

        private void MinusButton_Click(object sender, EventArgs e)
        {
            if (OutputBox.Text != "")
            {
                num1 = float.Parse(OutputBox.Text);
                OutputBox.Clear();
                OutputBox.Focus();
                count = 1;
            }
        }

        private void OneButton_Click(object sender, EventArgs e)
        {
            OutputBox.Text = OutputBox.Text + 1;
        }

        private void TwoButton_Click(object sender, EventArgs e)
        {
            OutputBox.Text = OutputBox.Text + 2;
        }

        private void FiveButton_Click(object sender, EventArgs e)
        {
            OutputBox.Text = OutputBox.Text + 5;
        }


        private void SevenButton_Click(object sender, EventArgs e)
        {
            OutputBox.Text = OutputBox.Text + 7;
        }

        private void EightButton_Click(object sender, EventArgs e)
        {
            OutputBox.Text = OutputBox.Text + 8;
        }

        private void NineButton_Click(object sender, EventArgs e)
        {
            OutputBox.Text = OutputBox.Text + 9;
        }

        private void ThreeButton_Click(object sender, EventArgs e)
        {
            OutputBox.Text = OutputBox.Text + 3;
        }

        private void FourButton_Click(object sender, EventArgs e)
        {
            OutputBox.Text = OutputBox.Text + 4;
        }

        private void SixButton_Click(object sender, EventArgs e)
        {
            OutputBox.Text = OutputBox.Text + 6;
        }

        private void DivideButton_Click(object sender, EventArgs e)
        {
            num1 = float.Parse(OutputBox.Text);
            OutputBox.Clear();
            OutputBox.Focus();
            count = 4; 
        }

        private void ZeroButton_Click(object sender, EventArgs e)
        {
            OutputBox.Text = OutputBox.Text + 0;
        }

        private void PlusButton_Click(object sender, EventArgs e)
        {
            num1 = float.Parse(OutputBox.Text);
            OutputBox.Clear();
            OutputBox.Focus();
            count = 2;
        }

        private void MultiplyButton_Click(object sender, EventArgs e)
        {
            num1 = float.Parse(OutputBox.Text);
            OutputBox.Clear();
            OutputBox.Focus();
            count = 3;
        }

        public void compute(int count)
        {
            switch (count)
            {
                case 1:
                    ans = num1 - float.Parse(OutputBox.Text);
                    OutputBox.Text = ans.ToString();
                    break;
                case 2:
                    ans = num1 + float.Parse(OutputBox.Text);
                    OutputBox.Text = ans.ToString();
                    break;
                case 3:
                    ans = num1 * float.Parse(OutputBox.Text);
                    break;
            }
        }

        private void EqualButton_Click(object sender, EventArgs e)
        {
            compute(count);
        }
    }
}
